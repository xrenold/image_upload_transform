var express = require('express');
var router = express.Router();
var multer = require('multer')
var sharp = require('sharp')

const multerStorage = multer.memoryStorage()

const multerFilter = (req, file, cb) => {
	if (file.mimetype.startsWith("image")) {
		cb(null, true);
	} else {
		cb("Please upload only images.", false);
	}
};

const upload = multer({
	storage: multerStorage,
	fileFilter: multerFilter
});

const uploadFiles = upload.array("images", 10);

const uploadImages = (req, res, next) => {
	uploadFiles(req, res, err => {
		if (err instanceof multer.MulterError) { // A Multer error occurred when uploading.
			if (err.code === "LIMIT_UNEXPECTED_FILE") { // Too many images exceeding the allowed limit
				// ...
			}
		} else if (err) {
			// handle other errors
		}

		// Everything is ok.
		next();
	});
};

const resizeImages = async (req, res, next) => {
	if(!req.files) next()

	req.body.images = []
	await Promise.all(
		req.files.map(async file => {
			try {
				console.log(file)
				await sharp(file.buffer)  // .toBuffer to get buffer output
					.toFormat("jpg")
					.toFile(`/home/toobler/Desktop/prototypes/image_upload_transform/public/images/${file.originalname}_original`)
				await sharp(file.buffer)
					.resize(640, 320)
					.toFormat("jpg")
					.jpeg({quality: 90})
					.toFile(`/home/toobler/Desktop/prototypes/image_upload_transform/public/images/${file.originalname}_profile`)
			}
			catch(err) {
				console.error("resize error", err)
			}
		})
	)
	next()
}

/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', { title: 'Express' });
});

router.post('/upload', uploadImages, resizeImages, function (req, res) {
	return res.status(200).json({ success: true })
})

module.exports = router;
